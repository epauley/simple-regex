Simple regex engine for python.

Implements simple character classes (no ranges), repetition metacharacters (+?\*), wildcard ".", and use of parentheses. "|" logic is included, but only works on the item to its left and right, which is either a single character class, or a entire parenthesized object. Instead of doing "cat|dog" which would match the strings "catog" or "cadog", you need to use "(cat)|(dog)" which matches "cat" or "dog". I prefer the "|" to work this way.

Use of nested parentheses only supported up to the recursion depth in Python.

There are no graph reductions, or transformation into a DFA, so the speed of matching will be subpar. Escape sequences are supported, but are very limited.
