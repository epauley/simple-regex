import sys
import copy

nodeCount = 0
debug = False
def epsilonEnclosure(node):
	frontier = [node]
	visited = [node]
	while frontier:
		current = frontier.pop()
		debugPrint("Popping '%s %d'" % (current.name,current.id))
		for vertex in current.paths:
			debugPrint("\t'%s %d'" % (vertex.name,vertex.id))
		visited.append(current)
		if(current.token is None):
			for v in current.paths:
				if(v not in visited and v not in frontier):
					debugPrint("Going from '%s %d' to '%s %d'" % (current.name,current.id,v.name,v.id))
					frontier.append(v)
	return visited

	
def thompsonWalker(str,root):
	states = {}
	return _thompsonWalker(str,0,root,states)
	
def _thompsonWalker(str,index,start,states):
	currentNode = start
	while(index < len(str) + 1):
		if(index < len(str)):
			currentChar = str[index]
		else:
			currentChar = "EOF"
			
		debugPrint("index %d: char '%s' node='%s %d'" % (index,currentChar,currentNode.name,currentNode.id))
		
		#Base case
		if(currentNode.name == "GOAL" and currentChar == "EOF"):
			return True
		
		
		#Check if we are at a brace decision node
		if(currentNode.braceDecisionNode):
			if(currentNode.counter+1 < currentNode.braceUpperBound[0]):
				debugPrint("Branching in curly brace counter=%d" % (currentNode.counter+1))
				currentNode.counter += 1
				result = _thompsonWalker(str,index,currentNode.braceLowerBound[1],states)
				if(result):
					return True
				currentNode.counter -= 1

			if(currentNode.counter+1 >= currentNode.braceLowerBound[0]):
				debugPrint("Branching to exit curly brace counter=%d->0" % (currentNode.counter+1))
				holdCounter = currentNode.counter
				currentNode.counter = 0
				result = _thompsonWalker(str,index,currentNode.braceUpperBound[1],states)
				if(result):
					return True
				currentNode.counter = holdCounter
				
			return False
				
		#See if this state has been in before, fail out if true
		if((index,currentNode.id) in states):
			return False
		else:
			states[(index,currentNode.id)] = 1
		
		#Greedy traverse
		targetNodes = []
		epsilonNodes = []
		for node in currentNode.paths:
			if(node.token is None):
				epsilonNodes.append(node)
			elif(node.match(currentChar)):
				targetNodes.append(node)
		
		#No possible transitions, fail out
		if(len(targetNodes) + len(epsilonNodes) == 0):
			return False
		
		#Transitions are possible
		if(len(targetNodes) + len(epsilonNodes) == 1):
			#linear case, just advance without branching or using stack
			if(len(epsilonNodes) > 0):
				debugPrint("linear: moving to '%s %d'" % (epsilonNodes[0].name,epsilonNodes[0].id))
				currentNode = epsilonNodes[0]
			else:
				debugPrint("linear: matching '%s' and moving to '%s %d'" % (currentChar,targetNodes[0].name,targetNodes[0].id))
				currentNode = targetNodes[0]
				index += 1
		else:
			#If we have a valid transition we branch
			for node in targetNodes:
				debugPrint("matching '%s' and moving to '%s %d'" % (currentChar,node.name,node.id))
				result = _thompsonWalker(str,index+1,node,states)
				if(result):
					return True
					
			#Do the same, but for epsilon nodes
			for node in epsilonNodes:
				debugPrint("moving to '%s %d'" % (node.name,node.id))
				result = _thompsonWalker(str,index,node,states)
				if(result):
					return True
			
		
	return False

class Construct:
	head = None
	tail = None
	name = None
	def __init__(self,name):
		self.name = name
		
	def __str__(self):
		return "[Con:" + self.name + "]"


class ThompsonNode:
	def __init__(self,name):
		global nodeCount
		self.paths = []
		self.token = None
		self.id = -1
		self.name = name
		self.id = nodeCount
		self.counter = 0
		
		#Information for curly brace matching
		self.braceDecisionNode = False
		self.braceLowerBound = None
		self.braceUpperBound = None
		nodeCount += 1
	def addPath(self,node):
		self.paths.append(node)
	def match(self,symbol):
		if(self.token is None):
			return False
		else:
			return self.token.match(symbol)

def thompsonBuilder(tokens):
	rootNode = Token("Node",ThompsonNode("ROOT"))
	goalNode = Token("Node",ThompsonNode("GOAL"))
	lastNode = None
	"""
	for token in tokens:
		if(token.type == "CharClass"):
			newNode = ThompsonNode("CharClass")
			newNode.token = token.token
			newNode.name = token.raw
			currentConstruct = makeSingleConstruct(newNode)
			constructs.append(currentConstruct)
		#in ["*","+","|","(",")","?"]
		elif(token.type == "Meta"):
			constructs.append(token)
		else:
			debugPrint(token)
			raise Exception("Invalid token")
	"""
			
	#Append root and goal node
	constructs = [rootNode] + tokens + [goalNode]
	
	#Create construct tree using paren information
	parenTree = createParenTree(constructs)
	
	#Process OR nodes by traversing paren tree
	constructGraph = processParenTree(parenTree)
	
	#Return root node
	return constructGraph.head
	
def createParenTree(tokens):
	debugPrint("Starting paren tree creation")
	#First element of each parenTree is the parent node
	#The root node is set to None
	parenTree = [None]
	current = parenTree
	depth = 0
	for token in tokens:
		if(isinstance(token,Token)):
			if(token.type == "Meta"):
				if(token.token == "("):
					debugPrint(depth*"\t" + "Open paren")
					depth += 1
					current.append([current])
					#Step into child
					current = current[-1]
				elif(token.token == ")"):
					depth -= 1
					debugPrint(depth*"\t" + "Close paren")
					#Go back to parent
					if(current[0] is None):
						raise Exception("Paren parsing error, to many ')'")
					current = current[0]
				else:
					current.append(token)
			elif(token.type == "CharClass"):
				newNode = ThompsonNode(token.type)
				newNode.token = token.token
				current.append(makeSingleConstruct(newNode))
			elif(token.type == "Node"):
				current.append(makeSingleConstruct(token.token))
			elif(token.type == "CurlyBrace"):
				current.append(token)
			else:
				print("ERROR, UNKNOWN TOKEN")
		else:
			print("NOT INSTANCE OF TOKEN IN CREATE PAREN TREE")
			#Otherwise just insert the construct
			current.append(token)
			debugPrint(depth*"\t" + str(current[-1]))
	debugPrint("Finishing paren tree")
	return parenTree
	
def processParenTree(treeNode):
	debugPrint("OPEN PAREN")
	root = makeSingleConstruct(ThompsonNode("TREE HEAD"))
	tail = makeSingleConstruct(ThompsonNode("TREE TAIL"))
	index = 1
	unionList = [root]
	while(index < len(treeNode)):
		node = treeNode[index]
		if(type(node) == type([])):
			#This is another paren node
			#Process it recursively, and then add it to union list
			newCon = processParenTree(node)
			unionList.append(newCon)
			index += 1
		elif(isinstance(node,Token)):
			if(node.type == "CharClass"):
				debugPrint(node.token)
				#Regular node
				unionList.append(makeSingleConstruct(node.token))
				index += 1
			elif(node.type == "Meta"):
				if(node.token == "|"):
					#Create the or construct by looking ahead 1 node
					secondHalf = treeNode[index+1]
					if(type(secondHalf) == type([])):
						secondHalf = processParenTree(secondHalf)
					firstHalf = unionList.pop()
					unionList.append(makeOrConstruct(firstHalf,secondHalf))
					index += 2
				elif(node.token == "*"):
					lastItem = unionList.pop()
					unionList.append(makeStarConstruct(lastItem))
					index += 1
				elif(node.token == "+"):
					lastItem = unionList.pop()
					unionList.append(makePlusConstruct(lastItem))
					index += 1
				elif(node.token == "?"):
					lastItem = unionList.pop()
					unionList.append(makeQuestionConstruct(lastItem))
					index += 1
			elif(node.type == "CurlyBrace"):
				lastItem = unionList.pop()
				n1,n2 = node.token
				unionList.append(makeCurlyBraceConstruct(lastItem,n1,n2))
				index += 1
			else:
				print("UNKNOWN TOKEN IN PROCESS PAREN TREE")
		elif(isinstance(node,Construct)):
			unionList.append(node)
			index += 1
		else:
			print("UNKNOWN CONSTRUCT IN PROCESS PAREN TREE")
			print(node)
			print(type(node))
			input("...")
				
	unionList.append(tail)
	bigUnion = unionList[0]
	for con in unionList[1:]:
		debugPrint(bigUnion)
		debugPrint(con)
		bigUnion = makeUnionConstruct(bigUnion,con)
	debugPrint("CLOSE PAREN")
	return bigUnion
			
def makeCurlyBraceConstruct(construct,n1,n2):
	debugPrint("Creating Curly Brace construct for {%d,%d}" % (n1,n2))
	newConstruct = Construct("CURLY BRACE")
	newConstruct.head = ThompsonNode("CURLY BRACE HEAD")
	newConstruct.tail = ThompsonNode("CURLY BRACE TAIL")
	newConstruct.head.addPath(construct.head)
	
	
	preTail = ThompsonNode("CURLY BRACE DECISION NODE")
	preTail.braceDecisionNode = True
	preTail.braceLowerBound = (n1,construct.head)
	preTail.braceUpperBound = (n2,newConstruct.tail)
	
	if(n1 == 0):
		newConstruct.head.addPath(newConstruct.tail)
	
	construct.tail.addPath(preTail)
	return newConstruct

def makeQuestionConstruct(construct):
	debugPrint("Creating question construct")
	newConstruct = Construct("QUESTION")
	newConstruct.head = ThompsonNode("QUESTION HEAD")
	newConstruct.tail = ThompsonNode("QUESTION TAIL")
	newConstruct.head.addPath(construct.head)
	newConstruct.head.addPath(newConstruct.tail)
	construct.tail.addPath(newConstruct.tail)
	return newConstruct
	
def makeSingleConstruct(node):
	debugPrint("Creating single construct for node '%s'" % (node.name))
	newConstruct = Construct("SINGLE")
	newConstruct.head = ThompsonNode("SINGLE HEAD")
	newConstruct.tail = ThompsonNode("SINGLE TAIL")
	
	newConstruct.head.addPath(node)
	node.addPath(newConstruct.tail)
	return newConstruct
	
def makeStarConstruct(construct):
	debugPrint("Creating star construct")
	newConstruct = Construct("STAR")
	newConstruct.head = ThompsonNode("STAR HEAD")
	newConstruct.tail = ThompsonNode("STAR TAIL")
	
	newConstruct.head.addPath(construct.head)
	newConstruct.head.addPath(newConstruct.tail)
	construct.tail.addPath(newConstruct.tail)
	newConstruct.tail.addPath(construct.head)
	return newConstruct
	
def makePlusConstruct(construct):
	debugPrint("Creating plus construct")
	newConstruct = Construct("PLUS")
	newConstruct.head = ThompsonNode("PLUS HEAD")
	newConstruct.tail = ThompsonNode("PLUS TAIL")
	newConstruct.head.addPath(construct.head)
	newConstruct.tail.addPath(construct.head)
	construct.tail.addPath(newConstruct.tail)
	return newConstruct
	
def makeOrConstruct(constructA,constructB):
	debugPrint("Creating OR construct")
	newConstruct = Construct("OR")
	newConstruct.head = ThompsonNode("OR HEAD")
	newConstruct.tail = ThompsonNode("OR TAIL")
	
	newConstruct.head.addPath(constructA.head)
	newConstruct.head.addPath(constructB.head)
	constructA.tail.addPath(newConstruct.tail)
	constructB.tail.addPath(newConstruct.tail)
	return newConstruct
	
def makeUnionConstruct(constructA,constructB):
	debugPrint("Creating union construct '%s' and '%s'" % (constructA.name,constructB.name))
	newConstruct = Construct("UNION")
	newConstruct.head = constructA.head
	newConstruct.tail = constructB.tail
	#Link A and B
	constructA.tail.addPath(constructB.head)
	return newConstruct
	
def serializeConstructs(constructA,constructB):
	debugPrint("Serializing constructs '%s' and '%s'" % (constructA.name,constructB.name))
	constructA.tail = constructB.head
	
			
#Naive character class with no range implementation
class CharClass:
	def __init__(self,str,negate=False,alwaysMatch=False):
		self.raw = str
		self.negate = negate
		self.alwaysMatch = alwaysMatch
		self.characters = set()
		if(len(str)==0):
			return
		for c in str:
			self.characters.add(c)
		if(self.negate):
			debugPrint("Creating negated character class for '%s'" % (self.raw))
		else:
			debugPrint("Creating character class for '" + str +"'")
	def match(self,c):
		debugPrint("Checking if '%s' matches character class for '%s'" % (c,self.raw))
		if(self.alwaysMatch):
			return True
		if(self.negate):
			debugPrint("result will be negated")
		if(c in self.characters):
			return not self.negate
		else:
			return self.negate
class Token:
	def __init__(self,type,token):
		self.type = type
		self.token = token

class Tokenizer:
	escapeDict = 	{	
						"n" : "\n",
						"s" : " \t",
						"d" : "0123456789",
						"r" : "\r"
					}
	
	def __init__(self,str):
		debugPrint("Starting tokenizer for string '%s'" % (str))
		self.tokens = []
		charIndex = 0
		charList = list(str)
		while charIndex < len(charList):
			currentChar = charList[charIndex]
			if(currentChar == "["):
				increment, token = self.readBracket(str,charIndex)
				token = Token("CharClass",token)
			elif(currentChar in ["*","+","?","|","(",")"]):
				debugPrint("Added meta character '%s'" % (currentChar))
				token = Token("Meta",currentChar)
				increment = 1
			elif(currentChar == "\\"):
				increment, token = self.readEscapeCharacter(str,charIndex)
				token = Token("CharClass",CharClass(token))
			elif(currentChar == "{"):
				increment, token = self.readCurlyBrace(str,charIndex)
				token = Token("CurlyBrace",token)
			else:
				increment, token = self.readSingleCharacter(str,charIndex)
				if(token == "."):
					token = CharClass(token,alwaysMatch=True)
				else:
					token = CharClass(token)
				token = Token("CharClass",token)
				
			self.tokens.append(token)
			charIndex += increment
	def readCurlyBrace(self,str,index):
		endIndex = index
		while(str[endIndex]!="}"):
			endIndex += 1
		braceText = str[index+1:endIndex]
		n1,n2 = braceText.split(",")
		n1 = int(n1)
		n2 = int(n2)
		if(n1 < 0 or n2 < 0):
			#TODO Brace error lt0
			pass
		elif(n1 > n2):
			#TODO Brace error
			pass
		result = (n1,n2)
		return endIndex-index+1, result
		
	def readBracket(self,str,index):
		endIndex = index
		chars = ""
		while(str[endIndex]!="]"):
			endIndex += 1
		bracketText = str[index+1:endIndex]
		
		#Replace special escape characters
		bracketProcessing = []
		bracketIndex = 0
		while(bracketIndex < len(bracketText)):
			currentChar = bracketText[bracketIndex]
			if(currentChar == "\\"):
				increment,token = self.readEscapeCharacter(bracketText,bracketIndex)
				bracketProcessing.append(token)
				bracketIndex += increment
			else:
				bracketProcessing.append(currentChar)
				bracketIndex += 1
		bracketText = "".join(bracketProcessing)
		result = None
		if(bracketText.startswith("^")):
			result = CharClass(bracketText[1:],negate=True)
		else:
			result = CharClass(bracketText)
		return endIndex-index+1,result
	
	def readSingleCharacter(self,str,index):
		result = str[index]
		return 1, result
		
	def readEscapeCharacter(self,str,index):
		result = None
		char = str[index+1]
		#TODO add support for escape sequences of length != 1
		if(char in self.escapeDict):
			debugPrint("escape character %s matches special case '%s'" % (char,self.escapeDict[char]))
			result = self.escapeDict[char]
		else:
			result = char
		return 2, result
def testing():
	lines = open("testing.txt","r").readlines()
	index = 0
	passedAllCases = True
	while(index < len(lines)):
		while(lines[index].strip() == ""):
			index += 1
		if(lines[index].startswith("#")):
			print(lines[index].strip())
			index += 1
			continue
		testCount = int(lines[index])
		index += 1
		regexStr = lines[index].strip()
		index += 1
		compiled = thompsonBuilder(Tokenizer(regexStr).tokens)
		print("REGEX:",regexStr)
		for testNumber in range(testCount):
			l = lines[index].strip()
			index += 1
			expectedResult,targetStr = l.split(" ",1)
			expectedResult = expectedResult.lower()
			result = thompsonWalker(targetStr,compiled)
			if(expectedResult == "true"):
				if(not result):
					print("FAIL %d:: Failed to match, %s" % (testNumber,targetStr))
					passedAllCases = False
					input("...")
				else:
					print("PASS %d:: matching %s" % (testNumber,targetStr))
			elif(expectedResult == "false"):
				if(result):
					print("FAIL %d:: Unexpected match, %s" % (testNumber,targetStr))
					passedAllCases = False
					input("...")
				else:
					print("PASS %d:: not matching %s" % (testNumber,targetStr))
			else:
				print("unknown boolean %s in test case file" % (expectedResult))
		if(passedAllCases):
			print("Passed all test cases")
		else:
			print("Did not pass all test cases")
		print("")
			
def debugPrint(s):
	global debug
	if(debug):
		print(s)
	
def main():
	if(len(sys.argv) >= 2):
		if(sys.argv[1] == "test"):
			testing()
			sys.exit(0)
	while(True):
		regexStr = input("Regex Str>")
		t = Tokenizer(regexStr)
		root = thompsonBuilder(t.tokens)
		try:
			while(True):
				i = input(">")
				print(thompsonWalker(i,root))
		except KeyboardInterrupt:
			print("\n\n")
			print("Enter new regex string")
	
if __name__ == "__main__":
	main()
